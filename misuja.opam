opam-version: "2.0"
maintainer: "Seb Mondet <seb@mondet.org>"
authors: "Seb Mondet <seb@mondet.org>"
homepage: "https://gitlab.com/smondet/misuja"
bug-reports: "https://gitlab.com/smondet/misuja/issues"
dev-repo: "git+https://gitlab.com/smondet/misuja.git"
license: "MIT"
build: [ "dune" "build" "-p" name "-j" jobs ]
depends: [
  "ocaml" {>= "4.03.0"}
  "dune" {build & >= "1.8.2"}
  "base-unix"
]
depexts: [
  [["ubuntu"] ["libjack-jackd2-dev"]]
  [["debian"] ["libjack-jackd2-dev"]]
  [["alpine"] ["jack-dev"]]
  [["fedora"] ["jack-audio-connection-kit-devel"]]
  # Those RPMs depend on the EPEL package repository:
  [["centos"] ["jack-audio-connection-kit-devel"]]
  [["rhel"] ["jack-audio-connection-kit-devel"]]
  # This one seems to require: "openSUSE Multimedia Libs"
  [["opensuse"] ["libjack-devel"]]
  # Cf. https://github.com/ocaml/opam-repository/pull/10167 for OSX:
  [["homebrew" "osx"] ["jack"]]
]
synopsis: "A library to use JACK-MIDI"
description: """
Misuja is a low-latency “MIDI communications thread” implemented in C
which is manipulated with an OCaml API: `Misuja.Sequencer.  (the
process communicates with the Sequencer thread through ring-buffers
provided by the Jack API).
"""
